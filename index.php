<!DOCTYPE html>
 <?php include 'db.php'; // seperti impport di java jadi semua atribut di db.php tersedia disini
    $sql = "select * from task "; // perintah sql untuk mengambil semua data di table task dari database crud di localhost

    $rows = $db->query($sql); // mengambil hasil tabel yang di proses menggunakan query di atas 

?> 
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CRUD app</title>

    <!-- import jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- import bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js" integrity="sha384-feJI7QwhOS+hwpX2zkaeJQjeiwlhOP+SdQDqhgvvo1DsjtiSQByFdThsxO669S2D" crossorigin="anonymous"></script>

</head>
<body>
    <div class="container">
        <div class="row ">
            <h1 class="center">To-Do List</h1>
            <div class="col">
            </div>
        </div>
        <div class="row">
            <button  type="button" class="btn btn-success" data-target="#myModal" data-toggle="modal">tambah</button>
            <button class="btn btn-danger">hapus</button>


      
            <!-- Trigger the modal with a button -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add new Task</h4>
      </div>
      <div class="modal-body">
        <form method="post" action="add.php"> <!--menggunakan post untuk mengirim ke add.php-->
            <div class="form-group">
                <label >Task Name</label>
                <input type="text" required name="task" class="form-control">

            </div>
            <input type="submit" name="send" value="send" class="btn btn-success " >
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

            
        
        </div>
        <br>
        
        <div class="row">
        <table class="table">
            <thead>
                <tr>
                <th scope="col">Id</th>
                <th scope="col" >Task</th>
                
                </tr>
            </thead>
            <tbody>
                <tr>
                <?php while($row = $rows->fetch_assoc()): ?> <!-- mengambil  semua rekord dari hasil query
                yang berupa associative array -->

                    <!-- <?php var_dump($row); ?>  test --> <!-- to test whether its printing everything in db -->
                    
                <th scope="row"><?php echo $row['id'] ?></th>
                <td class="col-md-10"><?php echo $row['name'] ?></td>
                <td><a href="" class="btn btn-success">Edit</a></td>
                <td><a href="" class="btn btn-danger">Delete</a></td>
                </tr>
                <?php endwhile; ?>
            </tbody>
        </table>
        </div>
    </div>
   
</body>
</html>